This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

SmartBrain is an image recognition app made with React, Node.js, Express.js, and PostgreSQL, as well as an external AI service called Clarifai. Sign up with your email and password, then submit the URL of a .jpg file and the app will outline the first face it detects with a blue box.
